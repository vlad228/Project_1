﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallMove : MonoBehaviour {

    public int BallJumpPower = 1000
        int BallMovePower = 500
     Rigidbody Ball;
    void Start () {
        Ball = this.gameObject.GetComponent<Rigidbody>();
	}
	

	void Update () {
        if (Input.GetKey(KeyCode.W))
            Ball.AddForce(Vector3.foward * BallJumpPower);
        if (Input.GetKey(KeyCode.D))
            Ball.AddForce(Vector3.right * BallJumpPower);
        if (Input.GetKey(KeyCode.S))
            Ball.AddForce(Vector3.back * BallJumpPower);
	}   
}
